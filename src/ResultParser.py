import requests
import json


class ResultParser:
    @staticmethod
    def getPage(link):
        if link is not None:
            try:
                responseJson = requests.get(link)
                if responseJson.status_code == 200:
                    try:
                        jsonDoc = json.loads(responseJson.text)

                        if jsonDoc["isCertFound"] is True:
                            certExpDate = jsonDoc["expiredAt"]
                        else:
                            certExpDate = None

                        return certExpDate
                    except:
                        return -1
                else:
                    return -2
            except:
                return -3

