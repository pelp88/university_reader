import math
import cv2
from datetime import datetime


class FaceRecog:
    face_cascade1 = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
    face_cascade2 = cv2.CascadeClassifier('lbpcascade_frontalface_improved.xml')

    def analyze(self, frame):
        faces1 = self.face_cascade1.detectMultiScale(frame, 1.1, 4)
        faces2 = self.face_cascade2.detectMultiScale(frame, 1.1, 4)

        for (x1, y1, z1, h1) in faces1:
            for (x2, y2, z2, h2) in faces2:
                print(f"[INFO, {datetime.now()}] Centers are {math.pow(x2 - x1, 2)} {math.pow(y2 - y1, 2)}")
                if math.fabs(math.pow(x2 - x1, 2) - math.pow(y2 - y1, 2)) < 15000:
                    print(f"[INFO, {datetime.now()}] Face detected")
                    cv2.rectangle(frame, (x1, y1), (x1 + z1, y1 + h1), (255, 0, 0), 2)
                    return frame
