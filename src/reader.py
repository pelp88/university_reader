import cv2
import time
import pyzbar.pyzbar as pyzbar
from datetime import datetime
from FaceRecog import FaceRecog


class Reader:
    # _cam = cv2.VideoCapture(0, cv2.CAP_DSHOW)
    # _cam.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
    # _cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)
    _cam = cv2.VideoCapture(0)

    def getFrame(self, colorFormat):
        frame = None

        if self._cam.isOpened():
            ret, frame = self._cam.read()
        else:
            ret = None

        if ret:
            frame = cv2.cvtColor(frame, colorFormat)
            return frame

    def release(self):
        self._cam.release()

    def takeFrame(self):
        image = self.getFrame(cv2.COLOR_BGR2GRAY)
        print(f"[INFO {datetime.now()}] Start search")
        if image is not None:
            try:
                barcodes = pyzbar.decode(image)
                if len(barcodes) > 0:
                    barcode = barcodes[-1]
                    (x, y, w, h) = barcode.rect
                    cv2.rectangle(image, (x, y), (x + w, y + h), (0, 0, 255), 2)
                    barcodeData = barcode.data.decode("utf-8")
                    link = barcodeData.replace("https://www.gosuslugi.ru/covid-cert/status/",
                                               "https://www.gosuslugi.ru/api/covid-cert-checker/v3/cert/status/")

                    link = link.replace("?lang=ru", "")

                    print(f"[INFO {datetime.now()}] Found QR Code: {link}")
                    print(f"[INFO {datetime.now()}] Started face recognition")

                    recog = FaceRecog()
                    image = recog.analyze(image)
                    if image is None:
                        pass

                    s = cv2.imwrite(rf'photo\photo-[{time.time()}].jpg', image)
                    print(f"[INFO {datetime.now()}] Image saved successfully" if s else
                          f"[ERROR {datetime.now()}] Error while saving image")
                    return link
                else:
                    return None
            except AssertionError:
                return None
