import sys
from datetime import datetime

from reader import Reader
from ResultParser import ResultParser

from PyQt6.QtWidgets import QMainWindow, QApplication
from PyQt6.QtCore import QThread, pyqtSignal, pyqtSlot, QTimer, QUrl, Qt
from PyQt6.QtMultimedia import QMediaPlayer, QAudioOutput
from PyQt6 import uic


class PhotoThread(QThread):
    changePixmap = pyqtSignal(str)
    timer = QTimer()
    scanner = Reader()

    def run(self):
        link = self.scanner.takeFrame()
        self.changePixmap.emit(link)
        self.exit()


class App(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi("main.ui", self)
        self.setWindowTitle("Covid QR scanner")
        self.statusLabel.setAlignment(Qt.AlignmentFlag.AlignBottom)
        self.thread = PhotoThread(self)
        self.stopButton.clicked.connect(self.stopCycle)
        self.thread.changePixmap.connect(self.setLink)
        self.timer1 = QTimer()
        self.timer2 = QTimer()
        self.pass_sound = QMediaPlayer()
        self.error_sound = QMediaPlayer()
        audio_output = QAudioOutput()
        audio_output.setVolume(50)
        self.pass_sound.setAudioOutput(audio_output)
        self.pass_sound.setSource(QUrl.fromLocalFile("pass.wav"))
        self.error_sound.setAudioOutput(audio_output)
        self.error_sound.setSource(QUrl.fromLocalFile("error.wav"))
        self.timer2.timeout.connect(self.doCycle)
        self.button.clicked.connect(lambda: self.timer2.start(1000))
        #self.button.setAlignment(Qt.AlignmentFlag.AlignBottom)
        #self.stopButton.setAlignment(Qt.AlignmentFlag.AlignBottom)
        self.flag = False

    def signal(self, fail):
        if fail == 1:
            self.setStyleSheet("background-color: green;")
            self.pass_sound.play()
        if fail == 0:
            self.setStyleSheet("background-color: yellow;")
            self.error_sound.play()
        elif fail <= -1:
            self.setStyleSheet("background-color: red;")
            self.error_sound.play()

        self.timer1.singleShot(3000, self.whiteAgain)

    def whiteAgain(self):
        self.setStyleSheet("background-color: white;")
        self.statusLabel.setText(" ")

    @pyqtSlot(str)
    def setLink(self, link):
        if link != '':
            try:
                print(link)
                result = ResultParser.getPage(link)

                if result is not None:
                    if result not in (-1, -2, -3):
                        date = datetime.strptime(result, "%d.%m.%Y")
                        if date > datetime.now():
                            res = f"✅ Сертификат действителен [{date}]!"
                            self.signal(1)
                        else:
                            res = f"🟡 Сертификат просрочен [{date}]!"
                            self.signal(0)
                    elif result == -1:
                        res = "❌ Ошибка обработки данных!"
                        self.signal(result)
                    elif result == -2:
                        res = "❌ Ошибка обработки данных, повторите сканирование!"
                        self.signal(result)
                    elif result == -3:
                        res = "❌ Ошибка загрузки данных, проверьте интернет-соединение!"
                        self.signal(result)
                else:
                    res = "❌ QR-код сертификата не найден или неверен!"
                    self.signal(-4)

                print(f"[INFO, {datetime.now()}] {res}")
                self.statusLabel.setText(res)
            except Exception as e:
                print(f"[ERROR, {datetime.now()}] Caught exception - {e}")

    def doCycle(self):
        if not self.flag:
            self.thread.run()
        else:
            self.flag = False
            self.timer2.stop()
            print(f"[INFO, {datetime.now()}] Scanner successfully stopped")

    def stopCycle(self):
        self.flag = True


def main():
    app = QApplication([])
    a = App()
    a.show()
    sys.exit(app.exec())


if __name__ == "__main__":
    main()
